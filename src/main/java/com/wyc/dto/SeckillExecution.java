package com.wyc.dto;


import com.wyc.dataobject.SuccessKilled;
import com.wyc.enums.SeckillStatEnum;
import lombok.Data;

/**
 * 封装执行秒杀后的结果:是否秒杀成功
 */
@Data
public class SeckillExecution {

    private long seckillId;

    //秒杀执行结果的状态
    private int state;

    //状态的明文标识
    private String stateInfo;

    //当秒杀成功时，需要传递秒杀成功的对象回去
    private SeckillDetail seckillDetail;

    //秒杀成功返回所有信息
    public SeckillExecution(long seckillId, SeckillStatEnum statEnum, SeckillDetail seckillDetail) {
        this.seckillId = seckillId;
        this.state = statEnum.getState();
        this.stateInfo = statEnum.getInfo();
        this.seckillDetail = seckillDetail;
    }

    //秒杀失败
    public SeckillExecution(long seckillId, SeckillStatEnum statEnum) {
        this.seckillId = seckillId;
        this.state = statEnum.getState();
        this.stateInfo = statEnum.getInfo();
    }


    @Override
    public String toString() {
        return "SeckillExecution{" +
                "seckillId=" + seckillId +
                ", state=" + state +
                ", stateInfo='" + stateInfo + '\'' +
                ", seckillDetail=" + seckillDetail +
                '}';
    }
}
