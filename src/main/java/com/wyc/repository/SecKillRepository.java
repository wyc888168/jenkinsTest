package com.wyc.repository;

import com.wyc.dataobject.Seckill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */

public interface SecKillRepository extends JpaRepository<Seckill, Long> {

    Seckill findByName(String name);

    List<Seckill> findByNameStartingWithAndNumberLessThan(String name, Integer number);

    List<Seckill> findByNameInOrNumberLessThan(String name, Integer number);

    @Query("select o from Seckill o where price=(select min(price) from Seckill)")
    Seckill getSeckillByPrice();

    @Query("select o from Seckill o where o.seckillId=?1 and o.number=?2")
    Seckill getParams1(Long seckillId, Integer number);

    @Query("select o from Seckill o where o.seckillId=:seckillId and o.number=:number")
    Seckill getParams2(@Param("seckillId") Long seckillId, @Param("number") Integer number);

    @Query("select o from Seckill o where o.name like %?1%")
    List<Seckill> queryLike1(String name);

    @Query("select o from Seckill o where o.name like %:name%")
    List<Seckill> queryLike2(@Param("name") String name);

    @Query(nativeQuery = true, value = "select count(1) from seckill")
    long getCount();

    @Transactional
    @Modifying
    @Query("update Seckill o set o.number=:number where o.seckillId=:seckillId")
    void update(@Param("seckillId") Long seckillId, @Param("number") Integer number);

}
