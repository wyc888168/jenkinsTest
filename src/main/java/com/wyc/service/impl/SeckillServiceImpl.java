package com.wyc.service.impl;

import com.wyc.dataobject.Seckill;
import com.wyc.dataobject.mapper.SeckillMapper;
import com.wyc.dataobject.mapper.SuccessSeckilledMapper;
import com.wyc.dto.Exposer;
import com.wyc.dto.SeckillDetail;
import com.wyc.dto.SeckillExecution;
import com.wyc.enums.SeckillStatEnum;
import com.wyc.exception.RepeatKillException;
import com.wyc.exception.SeckillCloseException;
import com.wyc.exception.SeckillException;
import com.wyc.repository.SecKillRepository;
import com.wyc.service.SeckillService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */
@Service
@Slf4j
public class SeckillServiceImpl implements SeckillService {

    private final String salt = "vdvbionpmoixc";

    @Autowired
    private SecKillRepository secKillRepository;

    @Autowired
    private SeckillMapper seckillMapper;

    @Autowired
    private SuccessSeckilledMapper mapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<Seckill> getSeckillList() {
        //添加到数据库
        List<Seckill> seckillList = secKillRepository.findAll();
        seckillList.forEach((seckill -> log.info("从数据库中添加信息 >> Seckill：" + seckill)));

        //添加到缓存
        String key = "Seckill_All";
        redisTemplate.opsForValue().set(key, seckillList, 1, TimeUnit.HOURS);
        seckillList.forEach((seckill -> log.info("从缓存中添加信息 >> Seckill：" + seckill)));
        return seckillList;
    }

    @Override
    public Seckill getSeckill(long seckillId) {

        // 从缓存中获取信息
        String key = "Seckill_" + seckillId;
        ValueOperations<String, Seckill> operations = redisTemplate.opsForValue();
        Seckill seckill = null;
        // 缓存存在
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) {
            seckill = operations.get(key);
            operations.set(key, seckill, 10, TimeUnit.HOURS);
            log.info("从缓存中获取信息 >> Seckill：" + seckill.toString());
            return seckill;
        }

        // 从 DB 中获取信息
        seckill = secKillRepository.findOne(seckillId);
        if (seckill == null) {
            throw new SeckillException("该商品不存在！");
        }
        log.info("从数据库中获取信息 >> Seckill：" + seckill.toString());

        return seckill;
    }

    @Override
    public Long addSeckill(Seckill seckill) {
        //添加到DB中
        secKillRepository.save(seckill);
        log.info("从数据库中添加信息 >> Seckill：" + seckill.toString());

        //添加到缓存中
        String key = "Seckill_" + seckill.getSeckillId();
        redisTemplate.opsForValue().set(key, seckill, 10, TimeUnit.HOURS);
        log.info("从缓存中添加信息 >> Seckill：" + seckill.toString());
        return seckill.getSeckillId();
    }


    @Override
    public Long updateSeckill(Seckill seckill) {
        Long seckillId = secKillRepository.save(seckill).getSeckillId();
        log.info("从数据库中更新信息 >> Seckill：" + seckill.toString());
        // 缓存存在，更新缓存信息
        String key = "Seckill_" + seckillId;
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) {
            ValueOperations<String, Seckill> operations = redisTemplate.opsForValue();
            operations.set(key, seckill, 10, TimeUnit.HOURS);
            log.info("从缓存中更新信息 >> Seckill：" + seckill.toString());
        }

        return seckillId;
    }

    @Override
    public Long deleteSeckill(long seckillId) {
        secKillRepository.delete(seckillId);
        log.info("从数据库中删除信息 >> seckillId：" + seckillId);
        // 缓存存在，删除缓存
        String key = "Seckill_" + seckillId;
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) {
            redisTemplate.delete(key);
            log.info("从缓存中删除信息 ID >> seckillId：" + seckillId);
        }
        return seckillId;
    }


    @Override
    public Exposer exportSeckillUrl(long seckillId) {
        Seckill seckill = this.getSeckill(seckillId);
        if (null == seckill) {
            return new Exposer(false, seckillId);
        }

        Date startTime = seckill.getStartTime();
        Date endTime = seckill.getEndTime();

        Date nowTime = new Date();
        if (startTime.getTime() > nowTime.getTime() || endTime.getTime() < nowTime.getTime()) {
            return new Exposer(false, seckillId, nowTime.getTime(), startTime.getTime(), endTime.getTime());
        }

        String md5 = getMD5(seckillId);
        return new Exposer(true, md5, seckillId);
    }

    private String getMD5(long seckillId) {
        String base = seckillId + "/" + salt;
        return DigestUtils.md5DigestAsHex(base.getBytes());
    }

    @Override
    @Transactional
    public SeckillExecution executeSeckill(long seckillId, String userPhone, String md5) throws SeckillException {
        long startTime = System.currentTimeMillis();
        if (md5 == null || !md5.equals(getMD5(seckillId))) {
            throw new SeckillException("数据被篡改！");
        }
        Seckill seckill = this.getSeckill(seckillId);
        if (seckill == null) {
            throw new SeckillException("该商品不存在！");
        }
        Date nowTime = new Date();
        try {
            int insertCount = mapper.insertSuccessKilled(seckillId, userPhone);
            if (insertCount <= 0) {
                throw new RepeatKillException("重复秒杀！");
            } else {
                int updateCount = seckillMapper.reduceNumber(seckillId, nowTime);
                if (updateCount <= 0) {
                    throw new SeckillCloseException("秒杀已关闭！");
                } else {
                    SeckillDetail seckillDetail = mapper.queryByIdWithSeckill(seckillId, userPhone);
                    log.info("秒杀成功！恭喜您获得: {}一台，请保持手机号码: {} 畅通,我们会尽快给您发货！", seckill.getName(), seckillDetail.getUserPhone());
                    long endTime = System.currentTimeMillis();
                    log.info("ExecuteSeckill()运行时间：" + (endTime - startTime) + "ms");
                    return new SeckillExecution(seckillId, SeckillStatEnum.SUCCESS, seckillDetail);
                }
            }
        } catch (RepeatKillException e) {
            log.error("重复秒杀错误！");
        } catch (SeckillCloseException e) {
            log.error("秒杀已关闭！");
        } catch (Exception e) {
            log.error("秒杀出错！");
        }
        return null;
    }

    @Override
    public SeckillExecution executeSeckillProcedure(long seckillId, String userPhone, String md5) {
        long startTime = System.currentTimeMillis();
        if (md5 == null || !md5.equals(getMD5(seckillId))) {
            return new SeckillExecution(seckillId, SeckillStatEnum.DATE_REWRITE);
        }
        Seckill seckill = this.getSeckill(seckillId);
        if (seckill == null) {
            throw new SeckillException("该商品不存在！");
        }
        Date killTime = new Date();
        Map<String, Object> map = new HashMap<>();
        map.put("seckillId", seckillId);
        map.put("phone", userPhone);
        map.put("killTime", killTime);
        map.put("result", null);
        // 执行储存过程,result被复制
        seckillMapper.killByProcedure(map);
        // 获取result
        int result = MapUtils.getInteger(map, "result", -2);
        if (result == 1) {
            SeckillDetail seckillDetail = mapper.queryByIdWithSeckill(seckillId, userPhone);
            log.info("秒杀成功！恭喜您获得: {}一台，请保持手机号码: {} 畅通,我们会尽快给您发货！", seckillDetail.getName(), seckillDetail.getUserPhone());
            long endTime = System.currentTimeMillis();
            log.info("ExecuteSeckillProcedure()运行时间：" + (endTime - startTime) + "ms");
            return new SeckillExecution(seckillId, SeckillStatEnum.SUCCESS, seckillDetail);
        } else {
            return new SeckillExecution(seckillId, SeckillStatEnum.stateOf(result));
        }
    }
}
