package com.wyc.dataobject.dao;


import com.wyc.dataobject.mapper.SeckillMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */
public class SeckillDao {

    @Autowired
    private SeckillMapper seckillMapper;

    public int reduceNumber(Long seckillId, Date killTime) {
        return seckillMapper.reduceNumber(seckillId, killTime);
    }

}
