package com.wyc.dataobject.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Date;
import java.util.Map;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */
public interface SeckillMapper {

    @Update("UPDATE seckill SET number = number-1  WHERE seckill_id=#{seckillId} AND start_time <= #{killTime} AND end_time >= #{killTime} AND number > 0")
    int  reduceNumber(@Param("seckillId") long seckillId, @Param("killTime") Date killTime);

    void killByProcedure(Map<String,Object> paramMap);
}
