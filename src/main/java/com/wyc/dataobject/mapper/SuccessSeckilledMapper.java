package com.wyc.dataobject.mapper;

import com.wyc.dto.SeckillDetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by weiyongchang
 * Description:
 * Date: 2017/10/31
 */
public interface SuccessSeckilledMapper {

    SeckillDetail queryByIdWithSeckill(@Param("seckillId") long seckillId, @Param("userPhone") String userPhone);

    int insertSuccessKilled(@Param("seckillId") long seckillId, @Param("userPhone") String userPhone);
}
